# Hazelcast nodes discovery inside of Kubernetes #

This is the thing I've been looking forward to test a lot. In the other words howto use [hazelcast](https://hazelcast.com/) IMDG inside of [Kubernetes](https://kubernetes.io/). Of course everything wrapped into the [Spring Boot](https://spring.io/projects/spring-boot) application. 
As usual. To use the hazelcast grid inside of Kubernetes go with [Hazelcast Discovery Plugin for Kubernetes](https://github.com/hazelcast/hazelcast-kubernetes). There are [two modes](https://github.com/hazelcast/hazelcast-kubernetes#understanding-discovery-modes) of howto use it to discover the nodes in Kubernetes, through:

* Kubernetes API
* DNS lookup

With the first option you need to actually allow your application to interact with Kubernetes API through [RBAC](https://kubernetes.io/docs/reference/access-authn-authz/rbac/) setting. Second option doesn't need any additional tweak, but "limits" you with using the headless-service
and statefulset when deploying the whole application. To be honest **I didn't find any complete example on the internet of using the DNS lookup mode** so let's create one. 

## DNS lookup demo prerequisities ##

* Minikube
* Ubuntu 18.04

## Spring Boot application with MVC controller using the Hazelcast ##

To test whether hazelcast works fine inside of Kubernetes let's create the following Spring MVC controller:

```java
@RestController
public class HazelcastController {

    @Autowired
    HazelcastInstance hazelcastInstance;

    @GetMapping(path = "/put")
    public String putToCache(@RequestParam(value = "key") String key,
                             @RequestParam(value = "value") String value) {
        IMap map = hazelcastInstance.getMap("test");
        map.put(key, value);

        return "Values pushes OK! \n";
    }

    @GetMapping("/get")
    public String get(@RequestParam(value = "key") String key) {
        final IMap<String, String> map = hazelcastInstance.getMap("test");
        final String value = map.get(key);
        final String hostName = System.getenv("HOSTNAME");
        return "Returned value: "+value +" from POD : "+hostName+"\n";
    }
}
```

## Kubernetes manifest and Hazelcast config with DNS Lookup mode ##

Of course first step is adding the Hazelcast Kubernetes discovery plugin as dependency:

```
		<dependency>
			<groupId>com.hazelcast</groupId>
			<artifactId>hazelcast-kubernetes</artifactId>
			<version>1.3.1</version>
		</dependency>
```

second step is to configure the Hazelcast instance by the following Spring Bean:

```java
    @Bean
    public Config hazelcastConfiguration() {
        Config config = new Config();
        JoinConfig joinConfig = config.getNetworkConfig().getJoin();
        joinConfig.getMulticastConfig().setEnabled(false);
        joinConfig.getKubernetesConfig().setEnabled(true)
                .setProperty("service-dns", "app-hzc-headless.default.svc.cluster.local");
        return config;
    }
```
With the service-dns property I'm saying here that Hazelcast cluster will be formed based on the [headless-service](https://kubernetes.io/docs/concepts/services-networking/service/#headless-services) with name **app-hzc-headless** in the **namespace default**.
Since our headless-service has to be named app-hzc-headless let's create the appropriate manifest, hazelcast default port is [5701](https://docs.hazelcast.org/docs/latest-dev/manual/html/ports.html):

```
apiVersion: v1
kind: Service
metadata:
  name: app-hzc-headless
  labels:
    app: app-hzc
spec:
  ports:
  - port: 5701
    name: app-hzc
  clusterIP: None
  selector:
    app: app-hzc
```

now the application POD deploy, we need to stick with [Statefulset](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/) as mentioned and metadata labels needs to match:

```
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mvchzc
  labels:
    app: app-hzc
spec:
  serviceName: "app-hzc-headless"
  selector:
    matchLabels:
      app: app-hzc
  replicas: 2
  template:
    metadata:
      labels:
        app: app-hzc
    spec:
      containers:
      - name: app-hzc
        image: kubernetes-hazelcast:0.0.1-SNAPSHOT
        ports:
        - containerPort: 8080
        - containerPort: 5701
```

But of course this isn't enough. We need to be able to invoke the Spring Boot application port 8080, so we need another kubernetes service with port exposure, I will go with [NodePort](https://kubernetes.io/docs/concepts/services-networking/service/#nodeport) again:

```
apiVersion: v1
kind: Service
metadata:
  name: app-hzc
  labels:
    name: app-hzc
spec:
  type: NodePort
  ports:
  - port: 8080
    targetPort: 8080
  selector:
    app: app-hzc
```
Notice the labels. They still need to match in all three kubernetes objects.

## Installing the Kubernetes manifests with the DNS lookup mode ##

* As always, switch your docker to the docker daemon inside of minikube

```
tomask79@utu:~/workspace/hazelcast/demo$ ls -l
total 40
-rw-rw-r-- 1 tomask79 tomask79  243 dub  8 21:06 Dockerfile
-rw-r--r-- 1 tomask79 tomask79  532 dub  7 07:36 HELP.md
drwxrwxr-x 2 tomask79 tomask79 4096 dub  9 23:02 kubernetes
-rwxr-xr-x 1 tomask79 tomask79 9114 dub  7 07:36 mvnw
-rw-r--r-- 1 tomask79 tomask79 5811 dub  7 07:36 mvnw.cmd
-rw-r--r-- 1 tomask79 tomask79 2110 dub  8 20:49 pom.xml
drwxr-xr-x 3 tomask79 tomask79 4096 dub 10 21:30 src
tomask79@utu:~/workspace/hazelcast/demo$ eval $(mnk docker-env)
```

* Package the application and build the docker image kubernetes-hazelcast:0.0.1-SNAPSHOT

```
tomask79@utu:~/workspace/hazelcast/demo$ mvn clean install
tomask79@utu:~/workspace/hazelcast/demo$ docker images
REPOSITORY                                                       TAG                 IMAGE ID            CREATED             SIZE
kubernetes-hazelcast                                             0.0.1-SNAPSHOT      c9423428a6ad        40 seconds ago      470MB
openjdk                                                          8-jre               d55d64383c12        2 weeks ago         443MB
openjdk                                                          <none>              ed287c436e66        2 months ago        443MB
gcr.io/kubernetes-helm/tiller                                    v2.11.0             ac5f7ee9ae7e        6 months ago        71.8MB
quay.io/kubernetes-ingress-controller/nginx-ingress-controller   0.19.0              22ebbdddfabb        7 months ago        414MB
k8s.gcr.io/coredns                                               1.2.2               367cdc8433a4        7 months ago        39.2MB
k8s.gcr.io/kubernetes-dashboard-amd64                            v1.10.0             0dab2435c100        7 months ago        122MB
k8s.gcr.io/kube-proxy-amd64                                      v1.10.0             bfc21aadc7d3        12 months ago       97MB
k8s.gcr.io/kube-scheduler-amd64                                  v1.10.0             704ba848e69a        12 months ago       50.4MB
k8s.gcr.io/kube-apiserver-amd64                                  v1.10.0             af20925d51a3        12 months ago       225MB
k8s.gcr.io/kube-controller-manager-amd64                         v1.10.0             ad86dbed1555        12 months ago       148MB
k8s.gcr.io/etcd-amd64                                            3.1.12              52920ad46f5b        13 months ago       193MB
k8s.gcr.io/kube-addon-manager                                    v8.6                9c16409588eb        13 months ago       78.4MB
registry.eit.zone/library/rhel7-hcibase-jdk8                     7.4-97-3-11         5beb4f5d1f67        15 months ago       440MB
k8s.gcr.io/k8s-dns-dnsmasq-nanny-amd64                           1.14.8              c2ce1ffb51ed        15 months ago       41MB
k8s.gcr.io/k8s-dns-sidecar-amd64                                 1.14.8              6f7f2dc7fab5        15 months ago       42.2MB
k8s.gcr.io/k8s-dns-kube-dns-amd64                                1.14.8              80cc5ea4b547        15 months ago       50.5MB
k8s.gcr.io/pause-amd64                                           3.1                 da86e6ba6ca1        15 months ago       742kB
gcr.io/k8s-minikube/storage-provisioner                          v1.8.1              4689081edb10        17 months ago       80.8MB
gcr.io/google_containers/defaultbackend                          1.4                 846921f0fe0e        17 months ago       4.84MB
```

* Docker image is ready, now we can install mentioned kubernetes objects. All are located in the ./kubernetes/manifests.yaml

```
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ kubectl apply -f manifests.yaml 
statefulset.apps/mvchzc created
service/app-hzc-headless created
service/app-hzc created
```

* If everything went well, this is the desired output:

```
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ kubectl get pods
NAME                                READY   STATUS             RESTARTS   AGE
mvchzc-0                            1/1     Running            0          3m
mvchzc-1                            1/1     Running            0          3m
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ kubectl get statefulsets
NAME     DESIRED   CURRENT   AGE
mvchzc   2         2         3m
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ kubectl get svc
NAME                TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
app-hzc             NodePort    10.102.63.35     <none>        8080:32247/TCP   3m
app-hzc-headless    ClusterIP   None             <none>        5701/TCP         3m
kubernetes          ClusterIP   10.96.0.1        <none>        443/TCP          70d
```
Notice the stable network ID of PODs.

## Testing the demo of DNS lookup mode ##

* First let's look inside of the started PODs on logs to see whether we're forming the Hazelcast cluster inside of Kubernetes

```
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ kubectl logs mvchzc-0
2019-04-11 19:35:38.674  INFO 1 --- [           main] com.hazelcast.core.LifecycleService      : [172.17.0.10]:5701 [dev] [3.11.2] [172.17.0.10]:5701 is STARTING
2019-04-11 19:35:38.742  INFO 1 --- [cached.thread-3] com.hazelcast.nio.tcp.TcpIpConnector     : [172.17.0.10]:5701 [dev] [3.11.2] Connecting to /172.17.0.11:5701, timeout: 0, bind-any: true
2019-04-11 19:35:39.540  INFO 1 --- [.IO.thread-in-1] com.hazelcast.nio.tcp.TcpIpConnection    : [172.17.0.10]:5701 [dev] [3.11.2] Initialized new cluster connection between /172.17.0.10:5701 and /172.17.0.11:40103
2019-04-11 19:35:39.569  INFO 1 --- [.IO.thread-in-0] com.hazelcast.nio.tcp.TcpIpConnection    : [172.17.0.10]:5701 [dev] [3.11.2] Initialized new cluster connection between /172.17.0.10:54603 and /172.17.0.11:5701
2019-04-11 19:35:44.767  INFO 1 --- [           main] c.h.internal.cluster.ClusterService      : [172.17.0.10]:5701 [dev] [3.11.2] 

Members {size:1, ver:1} [
	Member [172.17.0.10]:5701 - d29e57d0-3803-4e61-b863-63fc3d7c2a4e this
]

2019-04-11 19:35:44.806  INFO 1 --- [           main] com.hazelcast.core.LifecycleService      : [172.17.0.10]:5701 [dev] [3.11.2] [172.17.0.10]:5701 is STARTED
2019-04-11 19:35:45.037  INFO 1 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2019-04-11 19:35:45.382  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2019-04-11 19:35:45.388  INFO 1 --- [           main] com.example.demo.DemoApplication         : Started DemoApplication in 16.461 seconds (JVM running for 17.774)
2019-04-11 19:35:50.551  INFO 1 --- [ration.thread-0] c.h.internal.cluster.ClusterService      : [172.17.0.10]:5701 [dev] [3.11.2] 

Members {size:2, ver:2} [
	Member [172.17.0.10]:5701 - d29e57d0-3803-4e61-b863-63fc3d7c2a4e this
	Member [172.17.0.11]:5701 - 8d42140a-c946-4cbb-8b7e-75612766dbc4
]
```

same thing has to be seen also from POD mvchzc-1. If you see two members forming the hazelcast cluster you're good to go.

* Testing the Hazelcast inside of Kubernetes

```
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ mnk service app-hzc --url
http://192.168.99.100:32247
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ curl -X GET 'http://192.168.99.100:32247/put?key=test&value=hello'
Values pushes OK! 
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ curl -X GET 'http://192.168.99.100:32247/get?key=test'
Returned value: hello from POD : mvchzc-0
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ curl -X GET 'http://192.168.99.100:32247/get?key=test'
Returned value: hello from POD : mvchzc-0
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ curl -X GET 'http://192.168.99.100:32247/get?key=test'
Returned value: hello from POD : mvchzc-1
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ curl -X GET 'http://192.168.99.100:32247/get?key=test'
Returned value: hello from POD : mvchzc-0
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ curl -X GET 'http://192.168.99.100:32247/get?key=test'
Returned value: hello from POD : mvchzc-1
tomask79@utu:~/workspace/hazelcast/demo/kubernetes$ curl -X GET 'http://192.168.99.100:32247/get?key=test'
Returned value: hello from POD : mvchzc-1
```
As you can see Hazelcast works correctly, cached value is accessible on both Kubernetes PODs. Kubernetes DNS lookup discovery mode for Hazelcast works as expected.

Best Regards

Tomas


