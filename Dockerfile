FROM openjdk:8-jre
MAINTAINER Tomas Kloucek <tomas.kloucek@embedit.cz>

ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/myservice/myservice.jar"]

# Add the service itself
ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/myservice/myservice.jar