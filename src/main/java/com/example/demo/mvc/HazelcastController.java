package com.example.demo.mvc;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HazelcastController {

    @Autowired
    HazelcastInstance hazelcastInstance;

    @GetMapping(path = "/put")
    public String putToCache(@RequestParam(value = "key") String key,
                             @RequestParam(value = "value") String value) {
        IMap map = hazelcastInstance.getMap("test");
        map.put(key, value);

        return "Values pushes OK! \n";
    }

    @GetMapping("/get")
    public String get(@RequestParam(value = "key") String key) {
        final IMap<String, String> map = hazelcastInstance.getMap("test");
        final String value = map.get(key);
        final String hostName = System.getenv("HOSTNAME");
        return "Returned value: "+value +" from POD : "+hostName+"\n";
    }
}
